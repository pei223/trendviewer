package com.android.p2app.trendviewer.google_trend.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.android.p2app.trendviewer.R;
import com.android.p2app.trendviewer.common_interface.ContentFragment;

public class GoogleTrendPageFragment extends ContentFragment {
    private final String WEB_URL = "https://trends.google.co.jp/trends/trendingsearches/daily?geo=JP";
    WebView googleTrendWebView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_google_trend, container, false);
        initWebView(rootView);
        return rootView;
    }

    /**
     * WebViewの初期化、読み込みをする
     *
     * @param rootView
     */
    private void initWebView(View rootView) {
        googleTrendWebView = rootView.findViewById(R.id.google_trend_webview);
        setGoogleTrendWebView();
    }

    private void setGoogleTrendWebView() {
        googleTrendWebView.setWebViewClient(new GoogleTrendWebViewClient());
        googleTrendWebView.getSettings().setJavaScriptEnabled(true);
        googleTrendWebView.getSettings().setDomStorageEnabled(true);
        googleTrendWebView.loadUrl(WEB_URL);
    }

    @Override
    public void scrollToTop() {
        if (googleTrendWebView != null) {
            googleTrendWebView.scrollTo(0, 0);
        }
    }

    @Override
    public void reloadData() {
        if (googleTrendWebView != null) {
            setGoogleTrendWebView();
        }
    }
}
