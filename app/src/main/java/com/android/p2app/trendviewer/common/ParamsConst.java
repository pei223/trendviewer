package com.android.p2app.trendviewer.common;

/**
 * 共通で使用する定数を定義する
 */
public class ParamsConst {
    /**
     * プリファレンスのファイル名
     */
    public static final String PREFERENCE_FILE_NAME = "trendviewer_data";

    /**
     * Twitter APIのアクセストークン
     */
    public static final String TWITTER_API_ACCESS_TOKEN =
            "";
}
