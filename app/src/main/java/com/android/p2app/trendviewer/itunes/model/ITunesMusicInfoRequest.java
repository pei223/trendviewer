package com.android.p2app.trendviewer.itunes.model;

import android.util.Log;

import com.android.p2app.trendviewer.common.Contents;
import com.android.p2app.trendviewer.common.ResultId;
import com.android.p2app.trendviewer.common_interface.RequestToAccessAPI;
import com.android.p2app.trendviewer.itunes.entity.ITunesMusicInfoItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;

import static com.android.p2app.trendviewer.util.LogUtil.*;

public class ITunesMusicInfoRequest extends RequestToAccessAPI {
    final int CONNECTION_TIMEOUT_TIME_MS = 5000;
    final int READ_TIMEOUT_TIME_MS = 5000;

    @Override
    public Contents getContents() {
        return Contents.ITUNES;
    }

    @Override
    public int getApiAccessIntervalSec() {
        return 15;
    }

    /**
     * APIアクセス完了後に実行するコールバックインターフェース
     */
    public interface CallbackInterface {
        void onCallback(List<ITunesMusicInfoItem> entityList, ResultId resultId);
    }

    public void request(final ITunesMusicRequestId requestId, final CallbackInterface callback) {
        if (!canAccessApi(getPrefix(requestId))) {
            Log.i(getWrappedTagInStackTrace(INFO_TAG), "APIアクセスインターバル");
            notifyCacheData(requestId, callback);
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                notifyDataFromAPI(requestId, callback);
            }
        }).start();
    }

    /**
     * キャッシュデータをコールバックに通知する。
     *
     * @param requestId
     * @param callback
     */
    private void notifyCacheData(ITunesMusicRequestId requestId, CallbackInterface callback) {
        Log.i(getWrappedTagInStackTrace(INFO_TAG), "キャッシュアクセス");
        JSONObject cachedJson = getCache(getPrefix(requestId));
        // キャッシュがないためエラー通知
        if (cachedJson == null) {
            Log.e(getWrappedTagInStackTrace(API_ERROR_TAG), "キャッシュが存在しない");
            onCallback(callback, null, ResultId.API_ERROR_ID);
            return;
        }
        List<ITunesMusicInfoItem> entityList =
                ITunesMusicInfoItem.createListFromJson(cachedJson);
        if (entityList == null) {
            Log.e(getWrappedTagInStackTrace(NO_DATA_TAG), "キャッシュのパースエラー");
            onCallback(callback, null, ResultId.API_ERROR_ID);
            return;
        }
        onCallback(callback, entityList, ResultId.SUCCESS_RESULT_ID);
    }

    /**
     * APIアクセスして取得したデータをコールバックに通知する。
     *
     * @param requestId
     * @param callback
     */
    private void notifyDataFromAPI(ITunesMusicRequestId requestId, CallbackInterface callback) {
        try {
            String urlStr = getUrlFromRequestId(requestId);
            Log.i(getWrappedTagInStackTrace(API_ACCESS_TAG), "iTunes API Access : " + urlStr);
            HttpURLConnection conn = getConnectionToAccessITunes(urlStr);

            int statusCode = conn.getResponseCode();
            if (statusCode != HttpURLConnection.HTTP_OK) {
                Log.e(getWrappedTagInStackTrace(API_ERROR_TAG), "レスポンスがHTTP_OKではない: " + statusCode);
                notifyCacheData(requestId, callback);
                return;
            }
            JSONObject jsonData = getJsonFromConnection(conn);
            List<ITunesMusicInfoItem> entityList = ITunesMusicInfoItem.createListFromJson(jsonData);
            if (entityList == null) {
                Log.e(getWrappedTagInStackTrace(API_ERROR_TAG), "パースエラー");
                notifyCacheData(requestId, callback);
                return;
            }
            cacheData(jsonData, getPrefix(requestId));
            onCallback(callback, entityList, ResultId.SUCCESS_RESULT_ID);
            setTimeStamp(getPrefix(requestId));
        }
        // アクセス失敗したらキャッシュのデータを通知
        catch (SocketTimeoutException e) {
            Log.e(getWrappedTagInStackTrace(API_ERROR_TAG), "APIタイムアウト : " + e);
            notifyCacheData(requestId, callback);
        } catch (Exception e) {
            Log.e(getWrappedTagInStackTrace(API_ERROR_TAG), "未知のエラー : " + e);
            notifyCacheData(requestId, callback);
        }
    }

    /**
     * requestIdを使ってAPIのURLを判別して返す
     *
     * @param requestId
     * @return リクエストIDに対応したURL
     */
    private String getUrlFromRequestId(ITunesMusicRequestId requestId) {
        switch (requestId) {
            case ATTENTION_REQUEST_ID:
                return "https://rss.itunes.apple.com/api/v1/jp/itunes-music/hot-tracks/all/50/explicit.json";
            case NEW_MUSIC_REQUEST_ID:
                return "https://rss.itunes.apple.com/api/v1/jp/itunes-music/new-music/all/50/explicit.json";
            case TOP_ALBUM_REQUEST_ID:
                return "https://rss.itunes.apple.com/api/v1/jp/itunes-music/top-albums/all/50/explicit.json";
            case TOP_SONG_REQUEST_ID:
                return "https://rss.itunes.apple.com/api/v1/jp/itunes-music/top-songs/all/50/explicit.json";
            default:
                throw new RuntimeException("リクエストIDが無効");
        }
    }

    /**
     * iTunesの音楽データを取得するためのコネクションを取得
     *
     * @param urlStr iTunesのURL
     * @return
     * @throws IOException
     */
    private HttpURLConnection getConnectionToAccessITunes(String urlStr) throws IOException {
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        // ヘッダー、タイムアウトなどの設定
        conn.getHeaderField("content-type: application/json;charset=utf-8");
        conn.setConnectTimeout(CONNECTION_TIMEOUT_TIME_MS);
        conn.setReadTimeout(READ_TIMEOUT_TIME_MS);
        conn.setRequestMethod("GET");

        return conn;
    }

    /**
     * コネクションからJSONを取得する
     *
     * @param connectionToITunes
     * @return
     */
    private JSONObject getJsonFromConnection(HttpURLConnection connectionToITunes) {
        try (final InputStream in = connectionToITunes.getInputStream();
             final InputStreamReader inReader = new InputStreamReader(in);
             final BufferedReader bufferedReader = new BufferedReader(inReader)) {
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }
            return new JSONObject(result.toString());
        } catch (IOException | JSONException e) {
            Log.e(getWrappedTagInStackTrace(API_ERROR_TAG), e.getMessage());
            return null;
        }
    }

    private void onCallback(CallbackInterface callback,
                            List<ITunesMusicInfoItem> musicItemList, ResultId resultId) {
        if (callback != null) {
            callback.onCallback(musicItemList, resultId);
        }
    }

    /**
     * リクエストIDからファイル名に使用するPrefixを取得する
     *
     * @param requestId
     * @return
     */
    private String getPrefix(ITunesMusicRequestId requestId) {
        return String.valueOf(requestId.getId());
    }
}
