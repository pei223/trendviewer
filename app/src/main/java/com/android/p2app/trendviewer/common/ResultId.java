package com.android.p2app.trendviewer.common;

/**
 * 共通のリザルトID
 */
public enum ResultId {
    SUCCESS_RESULT_ID,
    API_ERROR_ID,
}
