package com.android.p2app.trendviewer.view;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import com.android.p2app.trendviewer.common.Contents;
import com.android.p2app.trendviewer.common_interface.ContentFragment;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static com.android.p2app.trendviewer.util.LogUtil.*;


public class TopFragmentPagerAdapter extends FragmentPagerAdapter {
    private ContentFragment selectedContent;

    public TopFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return Contents.values()[position].tabTitle;
    }

    @Override
    public Fragment getItem(int i) {
        // ReflectionでContentFragment継承オブジェクトを生成して返す
        Class<? extends ContentFragment> fragmentClass = Contents.values()[i].fragmentClass;
        try {
            Constructor<? extends ContentFragment> constructor = fragmentClass.getDeclaredConstructor();
            return constructor.newInstance();
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            Log.e(getWrappedTagInStackTrace(INTERNAL_ERROR_TAG), e.getMessage());
        }
        return null;
    }

    @Override
    public int getCount() {
        return Contents.values().length;
    }

    @Override
    public void setPrimaryItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        super.setPrimaryItem(container, position, object);
        if (object instanceof ContentFragment) {
            selectedContent = (ContentFragment) object;
        }
    }

    public void scrollToTop() {
        if (selectedContent != null) {
            selectedContent.scrollToTop();
        }
    }

    public void reloadContent() {
        if (selectedContent != null) {
            selectedContent.reloadData();
        }
    }
}
