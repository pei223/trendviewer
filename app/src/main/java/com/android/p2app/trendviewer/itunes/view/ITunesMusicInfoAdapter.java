package com.android.p2app.trendviewer.itunes.view;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.p2app.trendviewer.BR;
import com.android.p2app.trendviewer.R;
import com.android.p2app.trendviewer.itunes.viewmodel.ITunesMusicItemViewModel;

import java.util.List;

public class ITunesMusicInfoAdapter extends RecyclerView.Adapter<ITunesMusicInfoAdapter.MusicRankingViewHolder> {
    private List<ITunesMusicItemViewModel> itemList;
    private RecyclerView.LayoutManager layoutManager;

    ITunesMusicInfoAdapter(List<ITunesMusicItemViewModel> itemList) {
        this.itemList = itemList;
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @NonNull
    @Override
    public MusicRankingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // ViewHolderとViewModelをBindingする
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.itunes_music_item, viewGroup, false);
        return new MusicRankingViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MusicRankingViewHolder musicRankingViewHolder, int i) {
        // ViewHolderとViewModelをBindingする
        musicRankingViewHolder.bindViewModel(itemList.get(i));
    }

    @Override
    public void onViewRecycled(@NonNull MusicRankingViewHolder holder) {
        super.onViewRecycled(holder);
        clearThumbnail(holder);
        holder.bindViewModel(null);
    }

    /**
     * サムネイルをクリアする
     * @param holder
     */
    private void clearThumbnail(RecyclerView.ViewHolder holder) {
        ImageView thumbnailView = holder.itemView.findViewById(R.id.thumbnail);
        thumbnailView.setImageBitmap(null);
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    /**
     * リストを設定して再描画する
     *
     * @param itemList
     */
    public void setItemList(final List<ITunesMusicItemViewModel> itemList) {
        this.itemList = itemList;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                scrollToTop();
                notifyDataSetChanged();
            }
        });
    }

    /**
     * Viewの先頭へスクロールする
     */
    private void scrollToTop() {
        if (layoutManager == null) {
            throw new RuntimeException("LayoutManagerが初期化されていません");
        }
        layoutManager.scrollToPosition(0);
    }

    class MusicRankingViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding binding;

        MusicRankingViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        /**
         * ViewModelとbindさせる
         *
         * @param viewModel bindさせたいViewModel
         */
        void bindViewModel(ITunesMusicItemViewModel viewModel) {
            binding.setVariable(BR.viewModel, viewModel);
        }
    }
}
