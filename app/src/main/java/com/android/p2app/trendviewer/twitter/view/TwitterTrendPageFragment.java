package com.android.p2app.trendviewer.twitter.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.p2app.trendviewer.R;
import com.android.p2app.trendviewer.common_interface.ContentFragment;
import com.android.p2app.trendviewer.databinding.FragmentTwitterTrendBinding;
import com.android.p2app.trendviewer.twitter.viewmodel.TwitterTrendPageViewModel;

public class TwitterTrendPageFragment extends ContentFragment {
    RecyclerView mRecyclerView;
    TwitterTrendPageViewModel twitterTrendPageViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        TwitterTrendPageAdapter adapter = new TwitterTrendPageAdapter();

        twitterTrendPageViewModel = new TwitterTrendPageViewModel(adapter);
        FragmentTwitterTrendBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_twitter_trend,
                container, false);
        binding.setViewModel(twitterTrendPageViewModel);

        mRecyclerView = binding.getRoot().findViewById(R.id.tweet_recycler_view);
        mRecyclerView.setAdapter(adapter);

        twitterTrendPageViewModel.fetchTweetData();

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRecyclerView != null) {
            mRecyclerView.setAdapter(null);
            mRecyclerView = null;
        }
    }

    @Override
    public void scrollToTop() {
        if (mRecyclerView != null && mRecyclerView.getAdapter() != null) {
            mRecyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void reloadData() {
        if (twitterTrendPageViewModel != null) {
            twitterTrendPageViewModel.fetchTweetData();
        }
    }
}
