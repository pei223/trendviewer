package com.android.p2app.trendviewer.common_interface;

import android.support.v4.app.Fragment;

public abstract class ContentFragment extends Fragment {
    public abstract void scrollToTop();
    public abstract void reloadData();
}
