package com.android.p2app.trendviewer.itunes.entity;

import android.util.Log;


import com.android.p2app.trendviewer.util.LogUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import static com.android.p2app.trendviewer.util.LogUtil.*;


/**
 * iTunes音楽データ1こ分を示すクラス
 */
public class ITunesMusicInfoItem {
    public String artistName;
    public String musicName;
    public String releaseDate;
    public String thumbnailUrl;
    public String artistUrl;

    private ITunesMusicInfoItem(String artistName, String musicName, String releaseDate, String thumbnailUrl, String artistUrl) {
        this.artistName = artistName;
        this.musicName = musicName;
        this.releaseDate = releaseDate;
        this.thumbnailUrl = thumbnailUrl;
        this.artistUrl = artistUrl;
    }

    /**
     * JSONをパースしてiTunesデータのリストを取得する
     *
     * @param jsonObject パースしたいjsonオブジェクト
     * @return iTunesデータのリスト
     */
    public static List<ITunesMusicInfoItem> createListFromJson(JSONObject jsonObject) {
        if (jsonObject == null) {
            Log.e(getWrappedTagInStackTrace(NO_DATA_TAG), "iTunesリストを取得するためにパースするJSONが空");
            return null;
        }
        List<ITunesMusicInfoItem> data = new ArrayList<>();
        try {
            JSONArray results = jsonObject.getJSONObject("feed").getJSONArray("results");
            for (int i = 0; i < results.length(); i++) {
                ITunesMusicInfoItem item = createDataFromJson(results.getJSONObject(i));
                if (item != null) {
                    data.add(item);
                }
            }
            return data;
        } catch (JSONException e) {
            Log.e(getWrappedTagInStackTrace(INTERNAL_ERROR_TAG), "JSONパースエラー\n" + e);
            Log.e(getWrappedTagInStackTrace(INTERNAL_ERROR_TAG), jsonObject.toString());
        }
        return null;
    }

    /**
     * JSONをパースしてiTunesデータを取得する
     *
     * @param jsonObject パースしたいjsonオブジェクト
     * @return iTunesデータ
     */
    private static ITunesMusicInfoItem createDataFromJson(JSONObject jsonObject) {
        if (jsonObject == null) {
            Log.e(getWrappedTagInStackTrace(NO_DATA_TAG), "iTunesデータを取得するためにパースするJSONが空");
            return null;
        }
        try {
            String artistName = jsonObject.getString("artistName");
            String musicName = jsonObject.getString("name");
            String releaseDate = jsonObject.getString("releaseDate");
            String thumbnailUrl = jsonObject.getString("artworkUrl100");
            String artistUrl = jsonObject.getString("artistUrl");
            return new ITunesMusicInfoItem(artistName, musicName, releaseDate, thumbnailUrl, artistUrl);
        } catch (JSONException e) {
            Log.e(getWrappedTagInStackTrace(INTERNAL_ERROR_TAG), "JSONパースエラー\n" + e);
            Log.e(getWrappedTagInStackTrace(INTERNAL_ERROR_TAG), jsonObject.toString());
        }
        return null;
    }
}
