package com.android.p2app.trendviewer.common;

import com.android.p2app.trendviewer.R;
import com.android.p2app.trendviewer.common_interface.ContentFragment;
import com.android.p2app.trendviewer.google_trend.view.GoogleTrendPageFragment;
import com.android.p2app.trendviewer.itunes.view.ITunesMusicInfoFragment;
import com.android.p2app.trendviewer.twitter.view.TwitterTrendPageFragment;

/**
 * トレンドコンテンツのリスト
 */
public enum Contents {
    TWITTER(0, TwitterTrendPageFragment.class, "ツイート", R.drawable.tweet_tab_icon, "twitter_trend_timestamp", "twitter_trend"),
    GOOGLE_TREND(1, GoogleTrendPageFragment.class, "トレンド", R.drawable.google_tab_icon, "google_trend_timestamp", "google_trend"),
    ITUNES(2, ITunesMusicInfoFragment.class, "iTunes", R.drawable.itunes_tab_icon, "itunes_timestamp", "itunes");

    public final int number;
    public Class<? extends ContentFragment> fragmentClass;
    public String tabTitle;
    public int tabIconResourceId;
    public String APIAccessTimeStampKey;
    public String cacheFileName;

    Contents(final int number, Class<? extends ContentFragment> fragmentClass, String tabTitle, int tabIconResourceId,
             String APIAccessTimeStampKey, String cacheFileName) {
        this.number = number;
        this.fragmentClass = fragmentClass;
        this.tabTitle = tabTitle;
        this.tabIconResourceId = tabIconResourceId;
        this.APIAccessTimeStampKey = APIAccessTimeStampKey;
        this.cacheFileName = cacheFileName;
    }
}
