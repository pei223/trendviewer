package com.android.p2app.trendviewer.common_interface;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.p2app.trendviewer.MyApplication;
import com.android.p2app.trendviewer.common.Contents;
import com.android.p2app.trendviewer.common.ParamsConst;
import com.android.p2app.trendviewer.util.CacheUtil;
import com.android.p2app.trendviewer.util.LogUtil;

import org.json.JSONException;
import org.json.JSONObject;

import static com.android.p2app.trendviewer.util.LogUtil.*;


public abstract class RequestToAccessAPI {
    public abstract Contents getContents();

    public abstract int getApiAccessIntervalSec();


    protected void setTimeStamp() {
        setTimeStamp(null);
    }

    /**
     * タイムスタンプの更新
     */
    protected void setTimeStamp(String prefix) {
        String timestampKey = getContents().APIAccessTimeStampKey;
        if (prefix != null) {
            timestampKey += prefix;
        }
        SharedPreferences preferences =
                MyApplication.getInstance().getSharedPreferences(ParamsConst.PREFERENCE_FILE_NAME,
                        Context.MODE_PRIVATE);
        preferences.edit().putLong(timestampKey, System.currentTimeMillis()).apply();
    }

    protected boolean canAccessApi() {
        return canAccessApi(null);
    }

    /**
     * APIにアクセス可能かを返す
     *
     * @return アクセス可能ならtrue, そうでなければfalse
     */
    protected boolean canAccessApi(String prefix) {
        String timestampKey = getContents().APIAccessTimeStampKey;
        if (prefix != null) {
            timestampKey += prefix;
        }
        SharedPreferences preferences =
                MyApplication.getInstance().getSharedPreferences(ParamsConst.PREFERENCE_FILE_NAME,
                        Context.MODE_PRIVATE);
        long timeStamp = preferences.getLong(timestampKey, 0);
        boolean canAccessApi = timeStamp == 0 ||
                (System.currentTimeMillis() - timeStamp) / 1000 > getApiAccessIntervalSec();
        Log.i(getWrappedTagInStackTrace(INFO_TAG),
                "timestampKey: " + timestampKey + " - return " + canAccessApi);
        return canAccessApi;
    }

    protected void cacheData(JSONObject jsonData) {
        cacheData(jsonData, null);
    }

    /**
     * データをキャッシュする
     *
     * @param prefix コンテンツ内のキャッシュデータの接頭辞
     */
    protected void cacheData(JSONObject jsonData, String prefix) {
        String cacheFileName = getContents().cacheFileName;
        if (prefix != null) {
            cacheFileName += prefix;
        }
        CacheUtil.getInstance().saveStringToCache(cacheFileName, jsonData.toString());
    }

    protected JSONObject getCache() {
        return getCache(null);
    }

    /**
     * キャッシュデータを取得する
     *
     * @param prefix コンテンツ内のキャッシュデータの接頭辞
     * @return キャッシュデータ。なければnull。
     */
    protected JSONObject getCache(String prefix) {
        String cacheFileName = getContents().cacheFileName;
        if (prefix != null) {
            cacheFileName += prefix;
        }
        String cachedString = CacheUtil.getInstance().getStringFromCache(cacheFileName);
        try {
            return cachedString == null ? null : new JSONObject(cachedString);
        } catch (JSONException e) {
            Log.e(getWrappedTagInStackTrace(FILE_IO_ERROR_TAG), e.getMessage());
            return null;
        }
    }
}
