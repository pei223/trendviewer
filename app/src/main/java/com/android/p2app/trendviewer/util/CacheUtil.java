package com.android.p2app.trendviewer.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.android.p2app.trendviewer.MyApplication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.android.p2app.trendviewer.util.LogUtil.*;

public class CacheUtil {
    /**
     * 保存するキャッシュの最大値
     */
    private static final int CACHE_NUM_LIMIT = 100;

    /**
     * ファイル操作競合を防ぐためのLOCKオブジェクト
     */
    private final Object LOCK = new Object();

    private static CacheUtil instance = null;

    synchronized public static CacheUtil getInstance() {
        if (instance == null) {
            instance = new CacheUtil();
        }
        return instance;
    }

    /**
     * サムネイルのキャッシュを保存する
     *
     * @param url    サムネイルのURL
     * @param bitmap サムネイル
     */
    public void saveThumbnailCache(String url, Bitmap bitmap) {
        synchronized (LOCK) {
            // 古いファイルの削除
            File cacheDir = new File(getCacheDirPath());
            if (cacheDir.listFiles().length == CACHE_NUM_LIMIT) {
                File oldFile = null;
                long oldTimeStamp = Long.MAX_VALUE;
                // 更新日時が古いものを探索
                for (File cacheFile : cacheDir.listFiles()) {
                    if (oldTimeStamp > cacheFile.lastModified()) {
                        oldTimeStamp = cacheFile.lastModified();
                        oldFile = cacheFile;
                    }
                }
                if (oldFile != null) {
                    Log.d(getWrappedTagInStackTrace(INFO_TAG), "古いキャッシュの削除");
                    oldFile.delete();
                }
            }

            String filePath = getCacheDirPath() + "/" + getFileNameFromUrl(url);
            File thumbnailFile = new File(filePath);
            try (FileOutputStream fos = new FileOutputStream(thumbnailFile)) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            } catch (IOException e) {
                Log.e(getWrappedTagInStackTrace(INTERNAL_ERROR_TAG), "save cache failed : " + e);
                e.printStackTrace();
            }
        }
    }

    /**
     * サムネイルのキャッシュを取得する。キャッシュが存在しなければnullを返す
     *
     * @param url キャッシュのURL
     * @return サムネイルのキャッシュ。存在しなければnull
     */
    public Bitmap getThumbnailCache(String url) {
        synchronized (LOCK) {
            String filePath = getCacheDirPath() + "/" + getFileNameFromUrl(url);
            File thumbnailCache = new File(filePath);
            if (!thumbnailCache.exists()) {
                return null;
            }
            return BitmapFactory.decodeFile(filePath);
        }
    }

    /**
     * ハッシュ関数を用いてURLからファイル名を取得する
     *
     * @param url
     * @return
     */
    private String getFileNameFromUrl(String url) {
        MessageDigest md;
        StringBuilder sb;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Log.e(getWrappedTagInStackTrace(NO_DATA_TAG), "no such algorithm : " + e);
            return null;
        }
        md.update(url.getBytes());
        sb = new StringBuilder();
        for (byte b : md.digest()) {
            String hex = String.format("%02x", b);
            sb.append(hex);
        }
        return sb.toString();
    }

    /**
     * キャッシュフォルダのパスを取得する
     *
     * @return
     */
    private String getCacheDirPath() {
        return MyApplication.getInstance().getApplicationContext().
                getExternalCacheDir().getAbsolutePath();
    }

    /**
     * 文字列をキャッシュに保存する。存在する場合は上書きする。
     *
     * @param fileName 保存したいファイル名
     * @param text     保存したい文字列
     */
    public void saveStringToCache(String fileName, String text) {
        synchronized (LOCK) {
            Context context = MyApplication.getInstance().getApplicationContext();
            try (FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE)) {
                fos.write(text.getBytes());
            } catch (IOException e) {
                Log.e(getWrappedTagInStackTrace(FILE_IO_ERROR_TAG), e.getMessage());
            }
        }
    }

    /**
     * キャッシュファイルから文字列データを取得する。
     *
     * @param fileName キャッシュファイルの名前
     * @return キャッシュファイルから取得した文字列データ。存在しなければnull
     */
    public String getStringFromCache(String fileName) {
        synchronized (LOCK) {
            Context context = MyApplication.getInstance().getApplicationContext();
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(context.openFileInput(fileName)))) {
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                return sb.toString();
            } catch (IOException e) {
                Log.e(getWrappedTagInStackTrace(FILE_IO_ERROR_TAG), e.getMessage());
                return null;
            }
        }
    }
}
