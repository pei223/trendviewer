package com.android.p2app.trendviewer.view;

import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.android.p2app.trendviewer.R;
import com.android.p2app.trendviewer.common.Contents;
import com.android.p2app.trendviewer.util.NetUtil;

public class TopActivity extends AppCompatActivity {
    private ViewPager topFragmentPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top);

        if (!NetUtil.isOnline(this)) {
            showOffLineDialog();
            return;
        }
        // トップ画面のタブのセットアップ
        TopFragmentPagerAdapter adapter = new TopFragmentPagerAdapter(getSupportFragmentManager());
        topFragmentPager = findViewById(R.id.viewPager);
        topFragmentPager.setAdapter(adapter);

        setScrollButtonListener();
        setReloadButtonListener();

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(topFragmentPager);
        // タブのアイコンを設定
        for (Contents content : Contents.values()) {
            tabLayout.getTabAt(content.number).setIcon(content.tabIconResourceId);
        }
    }

    /**
     * オフラインで利用できないことを示すダイアログを表示する
     */
    private void showOffLineDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.offline_dialog_message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // OKを押したらアプリ終了
                        finish();
                    }
                })
                .show();
    }

    private void setScrollButtonListener() {
        findViewById(R.id.scroll_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (topFragmentPager.getAdapter() != null) {
                    ((TopFragmentPagerAdapter) topFragmentPager.getAdapter()).scrollToTop();
                }
            }
        });
    }

    private void setReloadButtonListener() {
        findViewById(R.id.reload_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TopFragmentPagerAdapter) topFragmentPager.getAdapter()).reloadContent();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        topFragmentPager.setAdapter(null);
        topFragmentPager = null;
    }
}
