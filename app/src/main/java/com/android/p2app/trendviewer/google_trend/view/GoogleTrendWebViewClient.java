package com.android.p2app.trendviewer.google_trend.view;

import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.p2app.trendviewer.R;
import com.android.p2app.trendviewer.util.LogUtil;

/**
 * WebView読み込み後にプログレスバーを消去するためのWebViewClient
 */
public class GoogleTrendWebViewClient extends WebViewClient {
    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        Log.d(LogUtil.API_ACCESS_TAG, "GoogleTrend Page Access Finished.");
        // プログレスバーを非表示にする
        view.getRootView().findViewById(R.id.progressbar).setVisibility(View.GONE);
    }
}
