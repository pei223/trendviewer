package com.android.p2app.trendviewer.itunes.viewmodel;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.p2app.trendviewer.itunes.entity.ITunesMusicInfoItem;
import com.android.p2app.trendviewer.util.CacheUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static com.android.p2app.trendviewer.util.LogUtil.*;

public class ITunesMusicItemViewModel extends BaseObservable {
    public ObservableField<String> artistName = new ObservableField<>();
    public ObservableField<String> musicName = new ObservableField<>();
    public ObservableField<String> releaseDate = new ObservableField<>();
    public ObservableField<String> thumbnailUrl = new ObservableField<>();
    private String artistUrl;

    /**
     * iTunes音楽データ1つ分からViewModelを生成する
     *
     * @param iTunesMusicItem
     */
    ITunesMusicItemViewModel(ITunesMusicInfoItem iTunesMusicItem) {
        this.artistName.set(iTunesMusicItem.artistName);
        this.musicName.set(iTunesMusicItem.musicName);
        this.releaseDate.set(iTunesMusicItem.releaseDate);
        this.thumbnailUrl.set(iTunesMusicItem.thumbnailUrl);
        this.artistUrl = iTunesMusicItem.artistUrl;
    }

    /**
     * アーティストページへリンクする
     *
     * @param view
     */
    public void linkArtistPage(View view) {
        Log.i(getWrappedTagInStackTrace(INFO_TAG), "アーティストページへリンク: " + artistUrl);

        Uri uri = Uri.parse(artistUrl);
        Intent linkPageIntent = new Intent(Intent.ACTION_VIEW, uri);
        view.getContext().startActivity(linkPageIntent);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("artistName=").append(artistName.get()).append("\n");
        sb.append("musicName=").append(musicName.get()).append("\n");
        sb.append("releaseDate=").append(releaseDate.get()).append("\n");
        sb.append("thumbnailUrl=").append(thumbnailUrl).append("\n");
        sb.append("artistUrl=").append(artistUrl).append("\n");
        return sb.toString();
    }

    /**
     * ImageViewのURLが設定されたら非同期でダウンロードして表示させる
     *
     * @param view
     * @param imageUrl
     */
    @BindingAdapter({"app:imageUrl"})
    public static void loadImage(final ImageView view, final String imageUrl) {
        if (imageUrl != null && !imageUrl.equals("")) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        boolean isThumbnailDownloaded = false;
                        Bitmap loadedThumbnail =
                                CacheUtil.getInstance().getThumbnailCache(imageUrl);
                        if (loadedThumbnail == null) {
                            // キャッシュがなければダウンロード
                            loadedThumbnail = BitmapFactory.decodeStream(
                                    (InputStream) new URL(imageUrl).getContent());
                            isThumbnailDownloaded = true;
                        }

                        final Bitmap finalLoadedThumbnail = loadedThumbnail;
                        view.post(new Runnable() {
                            @Override
                            public void run() {
                                view.setImageBitmap(finalLoadedThumbnail);
                            }
                        });
                        if (isThumbnailDownloaded) {
                            CacheUtil.getInstance().saveThumbnailCache(imageUrl, loadedThumbnail);
                        }
                    } catch (IOException e) {
                        Log.e(getWrappedTagInStackTrace(FILE_IO_ERROR_TAG), e.getMessage());
                    }
                }
            }).start();
        }
    }
}
