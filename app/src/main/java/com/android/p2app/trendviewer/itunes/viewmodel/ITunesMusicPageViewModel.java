package com.android.p2app.trendviewer.itunes.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.View;

import com.android.p2app.trendviewer.common.ResultId;
import com.android.p2app.trendviewer.itunes.entity.ITunesMusicInfoItem;
import com.android.p2app.trendviewer.itunes.model.ITunesMusicInfoRequest;
import com.android.p2app.trendviewer.itunes.model.ITunesMusicRequestId;
import com.android.p2app.trendviewer.itunes.view.ITunesMusicInfoAdapter;

import java.util.ArrayList;
import java.util.List;
import static com.android.p2app.trendviewer.util.LogUtil.*;


public class ITunesMusicPageViewModel extends BaseObservable {
    /**
     * プログレスバーが表示中かどうか
     */
    public ObservableBoolean isAccessingAPI = new ObservableBoolean(false);
    /**
     * アクセスに失敗したかどうか
     */
    public ObservableBoolean wasAccessFailed = new ObservableBoolean(false);
    private ITunesMusicInfoAdapter  adapter;
    /**
     * どの項目が選択されているか
     */
    public ObservableInt selectedItemNum = new ObservableInt(
            ITunesMusicRequestId.ATTENTION_REQUEST_ID.getId());

    public ITunesMusicPageViewModel(ITunesMusicInfoAdapter adapter) {
        this.adapter = adapter;
    }

    // ボタンからのアクションメソッド群
    public void onAttentionClick(View view) {
        changeMusicData(ITunesMusicRequestId.ATTENTION_REQUEST_ID);
    }

    public void onNewMusicClick(View view) {
        changeMusicData(ITunesMusicRequestId.NEW_MUSIC_REQUEST_ID);
    }

    public void onTopAlbumClick(View view) {
        changeMusicData(ITunesMusicRequestId.TOP_ALBUM_REQUEST_ID);
    }

    public void onTopSongClick(View view) {
        changeMusicData(ITunesMusicRequestId.TOP_SONG_REQUEST_ID);
    }

    /**
     * 受け取ったリクエストIDと対応した音楽データを取得して描画する
     *
     * @param requestId リクエストID
     */
    private void changeMusicData(ITunesMusicRequestId requestId) {
        // 他のアクセスが実行中ならなにもしない
        if (isAccessingAPI.get()) {
            Log.i(getWrappedTagInStackTrace(INFO_TAG), "アクセス中");
            return;
        }
        selectedItemNum.set(requestId.getId());
        fetchMusicData(requestId);
        selectedItemNum.notifyChange();
    }

    /**
     * iTunes音楽データを取得する
     */
    private void fetchMusicData(ITunesMusicRequestId requestId) {
        setAccessingStatus();
        new ITunesMusicInfoRequest().request(requestId, callback);
    }

    /**
     * リクエストID指定なしでiTunes音楽データを取得する
     * 注目データを取得する
     */
    public void fetchMusicData() {
        fetchMusicData(ITunesMusicRequestId.getRequestIdFromId(selectedItemNum.get()));
    }

    /**
     * iTunes音楽データを取得した後のコールバック
     */
    private ITunesMusicInfoRequest.CallbackInterface callback = new ITunesMusicInfoRequest.CallbackInterface() {
        @Override
        public void onCallback(List<ITunesMusicInfoItem> entityList, ResultId resultId) {
            Log.d(getWrappedTagInStackTrace(API_ACCESS_TAG),
                    entityList == null ? "" : entityList.toString());
            if (resultId.equals(ResultId.API_ERROR_ID) ||
                    entityList == null || entityList.size() == 0) {
                setAccessFailedStatus();
                return;
            }
            // adapterにリストをセットし再描画
            List<ITunesMusicItemViewModel> itemViewModelList = new ArrayList<>();
            for (ITunesMusicInfoItem item : entityList) {
                itemViewModelList.add(new ITunesMusicItemViewModel(item));
            }
            setAccessSucceedStatus();
            adapter.setItemList(itemViewModelList);
        }
    };

    private void setAccessFailedStatus() {
        isAccessingAPI.set(false);
        wasAccessFailed.set(true);
    }

    private void setAccessSucceedStatus() {
        isAccessingAPI.set(false);
        wasAccessFailed.set(false);
    }

    private void setAccessingStatus() {
        isAccessingAPI.set(true);
        wasAccessFailed.set(false);
    }
}
