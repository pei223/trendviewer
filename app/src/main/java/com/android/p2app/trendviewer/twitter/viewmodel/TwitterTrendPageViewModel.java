package com.android.p2app.trendviewer.twitter.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.util.Log;

import com.android.p2app.trendviewer.common.ResultId;
import com.android.p2app.trendviewer.twitter.model.TwitterTrendRequest;
import com.android.p2app.trendviewer.twitter.entity.TwitterTrendItem;
import com.android.p2app.trendviewer.twitter.view.TwitterTrendPageAdapter;

import static com.android.p2app.trendviewer.util.LogUtil.*;


import java.util.ArrayList;
import java.util.List;

public class TwitterTrendPageViewModel extends BaseObservable {
    /**
     * プログレスバーが表示中かどうか
     */
    public ObservableBoolean isAccessingAPI = new ObservableBoolean(false);
    /**
     * アクセスに失敗したかどうか
     */
    public ObservableBoolean wasAccessFailed = new ObservableBoolean(false);

    private TwitterTrendPageAdapter adapter;

    public TwitterTrendPageViewModel(TwitterTrendPageAdapter adapter) {
        this.adapter = adapter;
    }

    public void fetchTweetData() {
        setAccessingStatus();
        new TwitterTrendRequest().request(callback);
    }

    private TwitterTrendRequest.CallbackInterface callback =
            new TwitterTrendRequest.CallbackInterface() {
                @Override
                public void onCallback(List<TwitterTrendItem> twitterTrendList, ResultId resultId) {
                    Log.d(getWrappedTagInStackTrace(API_ACCESS_TAG), twitterTrendList == null ? "" : twitterTrendList.toString());
                    if (resultId == ResultId.API_ERROR_ID ||
                            twitterTrendList == null || twitterTrendList.size() == 0) {
                        setAccessFailedStatus();
                        return;
                    }
                    List<TwitterTrendItemViewModel> itemViewModelList = new ArrayList<>();
                    for (TwitterTrendItem trendWord : twitterTrendList) {
                        itemViewModelList.add(new TwitterTrendItemViewModel(trendWord));
                    }
                    setAccessSucceedStatus();
                    adapter.setItemList(itemViewModelList);
                }
            };

    private void setAccessFailedStatus() {
        isAccessingAPI.set(false);
        wasAccessFailed.set(true);
    }

    private void setAccessSucceedStatus() {
        isAccessingAPI.set(false);
        wasAccessFailed.set(false);
    }

    private void setAccessingStatus() {
        isAccessingAPI.set(true);
        wasAccessFailed.set(false);
    }
}
