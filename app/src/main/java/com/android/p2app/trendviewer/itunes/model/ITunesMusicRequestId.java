package com.android.p2app.trendviewer.itunes.model;

import android.util.Log;

import com.android.p2app.trendviewer.util.LogUtil;

/**
 * 音楽データ取得時のリクエストID
 */
public enum ITunesMusicRequestId {
    ATTENTION_REQUEST_ID(0),
    NEW_MUSIC_REQUEST_ID(1),
    TOP_ALBUM_REQUEST_ID(2),
    TOP_SONG_REQUEST_ID(3);

    private final int num;

    ITunesMusicRequestId(int i) {
        num = i;
    }

    public static ITunesMusicRequestId getRequestIdFromId(int id) {
        for (ITunesMusicRequestId requestId : ITunesMusicRequestId.values()) {
            if (requestId.getId() == id) {
                return requestId;
            }
        }
        Log.e(LogUtil.NO_DATA_TAG, "無効なID指定");
        return null;
    }

    public int getId() {
        return num;
    }
}
