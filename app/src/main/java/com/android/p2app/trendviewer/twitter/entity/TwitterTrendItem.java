package com.android.p2app.trendviewer.twitter.entity;

import android.util.Log;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import static com.android.p2app.trendviewer.util.LogUtil.*;


public class TwitterTrendItem {
    public String trendWord;
    public String url;

    private static final String JSON_KEY_CONTENTS = "trends";
    private static final String JSON_KEY_NAME = "name";
    private static final String JSON_KEY_URL = "url";

    public TwitterTrendItem(String trendWord, String url) {
        this.trendWord = trendWord;
        this.url = url;
    }

    /**
     * JSONをパースしてtwitterのトレンドワードデータのリストを取得する。
     * パースに失敗した場合はnullを返す。
     *
     * @param jsonObject パースしたいjson配列
     * @return twitterのトレンドワードデータのリスト。パースに失敗したらnull
     */
    public static List<TwitterTrendItem> createListFromJson(JSONObject jsonObject) {
        if (jsonObject == null) {
            Log.e(getWrappedTagInStackTrace(NO_DATA_TAG), "iTunesリストを取得するためにパースするJSONが空");
            return null;
        }
        try {
            List<TwitterTrendItem> twitterTrendWordList = new ArrayList<>();

            JSONArray contentArray = jsonObject.getJSONArray(JSON_KEY_CONTENTS);
            for (int i=0;i<contentArray.length();i++) {
                JSONObject content = contentArray.getJSONObject(i);
                String trendKeyword = content.getString(JSON_KEY_NAME);
                String trendKeywordUrl = content.getString(JSON_KEY_URL);
                twitterTrendWordList.add(new TwitterTrendItem(trendKeyword, trendKeywordUrl));
            }
            return twitterTrendWordList;
        }  catch (JSONException e) {
            Log.e(getWrappedTagInStackTrace(INTERNAL_ERROR_TAG), "JSONパースエラー\n" + e);
            Log.e(getWrappedTagInStackTrace(INTERNAL_ERROR_TAG), jsonObject.toString());
            return null;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("trendWord: " + trendWord).append("\n");
        sb.append("trendUrl: " + url).append("\n");
        return sb.toString();
    }
}
