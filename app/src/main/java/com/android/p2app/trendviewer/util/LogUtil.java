package com.android.p2app.trendviewer.util;

public class LogUtil {
    // エラー系タグ
    public static String API_ERROR_TAG = "[API_ERROR]";
    public static String INTERNAL_ERROR_TAG = "[INTERNAL_ERROR]";
    public static String NO_DATA_TAG = "[NO_DATA]";
    public static String FILE_IO_ERROR_TAG = "[FILE_I/O_ERROR]";

    // 通常ログ
    public static String INFO_TAG = "[INFO]";
    public static String API_ACCESS_TAG = "[API_ACCESS]";
    public static String ABOUD_VIEW_TAG = "[ABOUT_VIEW]";


    /**
     * StackTraceでラッピングしたログのタグを取得する
     * @param tag
     * @return
     */
    public static String getWrappedTagInStackTrace(String tag) {
        StackTraceElement e = Thread.currentThread().getStackTrace()[3];
        String formattedStackTrace =
                String.format("%-50s:",
                        "[" + getSimpleClassName(e.getClassName()) + "." +
                                e.getMethodName() + "#" + e.getLineNumber() + "]");
        return formattedStackTrace + " " + tag;
    }

    private static String getSimpleClassName(String fullClassName) {
        return fullClassName.substring(fullClassName.lastIndexOf('.') + 1, fullClassName.length());
    }
}
