package com.android.p2app.trendviewer.twitter.viewmodel;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.android.p2app.trendviewer.twitter.entity.TwitterTrendItem;
import static com.android.p2app.trendviewer.util.LogUtil.*;


public class TwitterTrendItemViewModel extends BaseObservable {
    public ObservableField<String> trendWord = new ObservableField<>();
    private String tweetUrl;

    TwitterTrendItemViewModel(TwitterTrendItem twitterTrendWord) {
        this.trendWord.set(twitterTrendWord.trendWord);
        this.tweetUrl = twitterTrendWord.url;
    }

    /**
     * 該当するTwitterトレンドワードのページへ遷移する
     * @param view
     */
    public void linkTwitterPage(View view) {
        Log.i(getWrappedTagInStackTrace(INFO_TAG), "twitterページへリンク: " + tweetUrl);
        Uri uri = Uri.parse(tweetUrl);
        Intent linkPageIntent = new Intent(Intent.ACTION_VIEW, uri);
        view.getContext().startActivity(linkPageIntent);
    }
}
