package com.android.p2app.trendviewer.itunes.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.p2app.trendviewer.R;
import com.android.p2app.trendviewer.common_interface.ContentFragment;
import com.android.p2app.trendviewer.databinding.FragmentMusicBinding;
import com.android.p2app.trendviewer.itunes.viewmodel.ITunesMusicPageViewModel;

public class ITunesMusicInfoFragment extends ContentFragment {
    RecyclerView mRecyclerView;
    ITunesMusicPageViewModel iTunesMusicPageViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ITunesMusicInfoAdapter adapter = new ITunesMusicInfoAdapter(null);

        iTunesMusicPageViewModel = new ITunesMusicPageViewModel(adapter);
        FragmentMusicBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_music,
                container, false);
        binding.setViewModel(iTunesMusicPageViewModel);

        mRecyclerView = binding.getRoot().findViewById(R.id.music_recycler_view);
        mRecyclerView.setAdapter(adapter);
        adapter.setLayoutManager(mRecyclerView.getLayoutManager());

        iTunesMusicPageViewModel.fetchMusicData();

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRecyclerView != null) {
            mRecyclerView.setAdapter(null);
            mRecyclerView = null;
        }
    }

    @Override
    public void scrollToTop() {
        if (mRecyclerView != null && mRecyclerView.getAdapter() != null) {
            mRecyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void reloadData() {
        if (iTunesMusicPageViewModel != null) {
            iTunesMusicPageViewModel.fetchMusicData();
        }
    }
}
