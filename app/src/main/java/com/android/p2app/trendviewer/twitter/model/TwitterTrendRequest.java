package com.android.p2app.trendviewer.twitter.model;

import android.util.Log;

import com.android.p2app.trendviewer.common.Contents;
import com.android.p2app.trendviewer.common.ParamsConst;
import com.android.p2app.trendviewer.common.ResultId;
import com.android.p2app.trendviewer.common_interface.RequestToAccessAPI;
import com.android.p2app.trendviewer.twitter.entity.TwitterTrendItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;
import static com.android.p2app.trendviewer.util.LogUtil.*;


public class TwitterTrendRequest extends RequestToAccessAPI {
    private final String API_URL = "https://api.twitter.com/1.1/trends/place.json?id=1118370";

    final int CONNECTION_TIMEOUT_TIME_MS = 5000;
    final int READ_TIMEOUT_TIME_MS = 5000;

    @Override
    public Contents getContents() {
        return Contents.TWITTER;
    }

    @Override
    public int getApiAccessIntervalSec() {
        return 15;
    }

    /**
     * APIアクセス完了後に実行するコールバックインターフェース
     */
    public interface CallbackInterface {
        void onCallback(List<TwitterTrendItem> twitterTrendList, ResultId resultId);
    }

    public void request(final CallbackInterface callback) {
        if (!canAccessApi()) {
            Log.i(getWrappedTagInStackTrace(INFO_TAG), "APIアクセスインターバル");
            notifyCacheData(callback);
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                notifyDataFromAPI(callback);
            }
        }).start();
    }

    /**
     * キャッシュデータをコールバックに通知する。
     *
     * @param callback
     */
    private void notifyCacheData(CallbackInterface callback) {
        Log.i(getWrappedTagInStackTrace(API_ACCESS_TAG), "APIアクセスインターバル中");
        JSONObject cachedJson = getCache();
        // キャッシュがないためエラー通知
        if (cachedJson == null) {
            Log.e(getWrappedTagInStackTrace(API_ERROR_TAG), "キャッシュが存在しない");
            onCallback(callback, null, ResultId.API_ERROR_ID);
            return;
        }
        List<TwitterTrendItem> entityList =
                TwitterTrendItem.createListFromJson(cachedJson);
        if (entityList == null) {
            Log.e(getWrappedTagInStackTrace(NO_DATA_TAG), "キャッシュのパースエラー");
            onCallback(callback, null, ResultId.API_ERROR_ID);
            return;
        }
        onCallback(callback, entityList, ResultId.SUCCESS_RESULT_ID);
    }

    /**
     * APIから取得したデータをコールバックに通知する。
     *
     * @param callback
     */
    private void notifyDataFromAPI(CallbackInterface callback) {
        try {
            HttpURLConnection conn = getConnectionToAccessTwitter();
            conn.connect();

            int statusCode = conn.getResponseCode();
            if (statusCode != HttpURLConnection.HTTP_OK) {
                Log.e(getWrappedTagInStackTrace(API_ERROR_TAG), "レスポンスがHTTP_OKではない: " + statusCode);
                notifyCacheData(callback);
                return;
            }
            JSONObject jsonData = getJsonFromConnection(conn);
            List<TwitterTrendItem> parsedTwitterTrendWordList =
                    TwitterTrendItem.createListFromJson(jsonData);
            if (parsedTwitterTrendWordList == null) {
                Log.e(getWrappedTagInStackTrace(API_ERROR_TAG), "パースエラー");
                notifyCacheData(callback);
                return;
            }
            cacheData(jsonData);
            onCallback(callback, parsedTwitterTrendWordList, ResultId.SUCCESS_RESULT_ID);
            setTimeStamp();
        }
        // 失敗したらキャッシュを通知
        catch (SocketTimeoutException e) {
            Log.e(getWrappedTagInStackTrace(API_ERROR_TAG), "APIタイムアウト: " + e);
            notifyCacheData(callback);
        } catch (Exception e) {
            Log.e(getWrappedTagInStackTrace(API_ERROR_TAG), "未知のエラー: " + e);
            notifyCacheData(callback);
        }
    }

    /**
     * Twitterトレンドワードを取得するためのコネクションを取得
     *
     * @return Twitterトレンドワードを取得するためのコネクション
     * @throws IOException
     */
    private HttpURLConnection getConnectionToAccessTwitter() throws IOException {
        Log.i(getWrappedTagInStackTrace(API_ACCESS_TAG), "Twitter API Access : " + API_URL);
        // ヘッダー、タイムアウトなどの設定
        URL url = new URL(API_URL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.addRequestProperty("Authorization", "Bearer " + ParamsConst.TWITTER_API_ACCESS_TOKEN);
        conn.setConnectTimeout(CONNECTION_TIMEOUT_TIME_MS);
        conn.setReadTimeout(READ_TIMEOUT_TIME_MS);
        return conn;
    }

    /**
     * コネクションからJSONを取得する
     *
     * @param connectionToAccessTwitter
     * @return
     */
    private JSONObject getJsonFromConnection(
            HttpURLConnection connectionToAccessTwitter) {
        try (final InputStream in = connectionToAccessTwitter.getInputStream();
             final InputStreamReader inReader = new InputStreamReader(in);
             final BufferedReader bufferedReader = new BufferedReader(inReader)) {
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }
            return new JSONArray(result.toString()).getJSONObject(0);
        } catch (JSONException | IOException e) {
            Log.e(getWrappedTagInStackTrace(API_ACCESS_TAG), "JSONパースエラー");
            return null;
        }
    }

    private void onCallback(CallbackInterface callback,
                            List<TwitterTrendItem> trendWordList, ResultId resultId) {
        if (callback != null) {
            callback.onCallback(trendWordList, resultId);
        }
    }
}
